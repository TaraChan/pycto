### What is Pycto? ###

The name "Pycto" was derived from the latin word "Picto" (picture) and "Cryptography".  
Pycto is a tiny command-line-utility used to save hidden messages inside of images by modifying the least significant bits of the pixels.  
The input image can be in any format that the FreeImage library supports.  
The output image is always in the PNG format, but the bit-depth depends on the input image.  
The text that should be encrypted must be in the ASCII format (for now).  

The code is intended to be as short as possible (but with proper error logging).

### Example ###

Image | Explanation
:----- | :-----
![pycto_icon_v1.png](https://bitbucket.org/repo/XAAy9q/images/3020992782-pycto_icon_v1.png)  |  This is the unchanged Pycto logo.
![pycto_icon_v1_encrypted.png](https://bitbucket.org/repo/XAAy9q/images/3119182662-pycto_icon_v1_encrypted.png)     |    **Here's the same logo, but with following text embedded in it (source: http://en.wikipedia.org/wiki/Steganography):**  *Steganography is the art or practice of concealing a message, image, or file within another message, image, or file. The word steganography combines the Ancient Greek words steganos, meaning "covered, concealed, or protected", and graphein meaning "writing". The first recorded use of the term was in 1499 by Johannes Trithemius in his Steganographia, a treatise on cryptography and steganography, disguised as a book on magic. Generally, the hidden messages will appear to be (or be part of) something else: images, articles, shopping lists, or some other cover text. For example, the hidden message may be in invisible ink between the visible lines of a private letter. Some implementations of steganography which lack a shared secret are forms of security through obscurity, whereas key-dependent steganographic schemes adhere to Kerckhoffs's principle.*
![pycto_icon_v1_difference.png](https://bitbucket.org/repo/XAAy9q/images/4251642936-pycto_icon_v1_difference.png)      |     This image shows the difference between the two above images (exaggerated).

### License ###
The code is free to use for non-commercial purposes, but please credit me.  
If you want to use it for commercial purposes, please contact me.

Also, this tool indirectly depends (through my collection of shared code) on the FreeImage library (http://freeimage.sourceforge.net/license.html).

### How do I get set up? ###

* The code should compile fine on Linux, Windows and OSX. So far it was only tested on Windows.
* This tool depends on my collection of shared code (will be released in the future), which uses the FreeImage library.
* At runtime the application only depends on the FreeImage DLL (and obviously the C++ runtime).

### Contribution guidelines ###

If you want to request a feature, simply create a new issue for it.

### Tests ###
Input image formats tested so far:
 
* 24 bit PNG
* 32 bit PNG
* 24 bit JPEG