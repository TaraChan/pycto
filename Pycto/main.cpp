// Pycto, a text to image de-/en-cryptor, made by Dudeson.

// Standard library:
#include <cstdio>
#include <vector>

// Shared:
#include <Utilities/IMAGE.H>
#include <Utilities/STRING.H>

std::string decrypt_string_from_image(UTILITIES::IMAGE& image)
{
	const unsigned char* pixel_data = image.get_data();
	const unsigned int pixel_data_length = image.get_width() * image.get_height() * image.get_bits_per_pixel() / 8;

	std::string string_buffer;
	unsigned int bit_mask = 1;
	unsigned char current_character = 0;

	for(unsigned int i=0; i<pixel_data_length; ++i)
	{
		// Set the bit we are currently working on correctly:
		current_character |= (pixel_data[i] & 1) * bit_mask;	// Multiplication with "bit_mask" to shift the bit to the correct position.
			
		bit_mask <<= 1;
		if(bit_mask > 128)	// If we already processed 8 bits, it means we have read a whole character:
		{
			string_buffer.push_back((char)current_character);
				
			if(current_character == NULL)	// If this character is the terminating NULL-character:
			{
				return(string_buffer);
			}

			current_character = 0;
			bit_mask = 1;
		}
	}

	return("");
}

bool encrypt_string_into_image(UTILITIES::IMAGE& image, const char* text)
{
	const unsigned int width = image.get_width();
	const unsigned int height = image.get_height();
	unsigned char* pixel_data = image.get_data();
	const unsigned int bytes_per_pixel = image.get_bits_per_pixel() / 8;
	const unsigned int pixel_data_length = image.get_width() * image.get_height() * bytes_per_pixel;

	unsigned int bit_mask = 1;
	const char* text_iterator = text;

	for(unsigned int i=0; i<pixel_data_length; ++i)
	{
		unsigned char* current_pixel_pointer = &pixel_data[i];

		*current_pixel_pointer = (*current_pixel_pointer & ~1) |	// This clears the first bit of the byte.
								 ((*text_iterator & bit_mask)	// This selects the bit we are currently working on using the bit mask.
								 / bit_mask);	// This shifts the result so we get either 0 or 1 depending on wether the selected bit (above) was set or not.
			
		bit_mask <<= 1;
		if(bit_mask > 128)	// All 8 bits of the current character have been written into the image. Continue with the next character:
		{
			if(*text_iterator == NULL)
			{
				return(true);	// String encrypted successfully.
			}

			++text_iterator;
			bit_mask = 1;
		}
	}
	
	printf("The text is too long to be stored inside the image!\n"
		   "The maximum text length for this image is %d characters, but the text was %d characters long.\n",
		   width * height * bytes_per_pixel / 8 - 1,	// "- 1" because the terminating NULL character requires a byte.
		   strlen(text));

	return(false);	// If we reached this spot, it means the string was too long to be fully stored inside the image.
}

std::string generate_output_file_path(std::string file_path)
{
	size_t extension_start_index = file_path.find_last_of('.');

	if(extension_start_index == std::string::npos)	// If no extension could be found:
	{
		extension_start_index = file_path.length();
	}

	file_path.insert(extension_start_index, "_encrypted");

	return(file_path);
}

bool decrypt(const char* image_file_path)
{
	UTILITIES::IMAGE image;
	if(!image.load(image_file_path))
	{
		printf("Failed to load the image!\n");
		return(false);
	}

	std::string decrypted_string = decrypt_string_from_image(image);

	if(decrypted_string.empty())
	{
		printf("Failed to decrypt the text!\n");
		return(false);
	}

	printf("Decrypted text: %s\n", decrypted_string.c_str());

	return(true);
}

bool encrypt(const char* image_file_path, const char* text)
{
	UTILITIES::IMAGE image;
	if(!image.load(image_file_path))
	{
		printf("Failed to load the image!\n");
		return(false);
	}

	if(!encrypt_string_into_image(image, text))
	{
		printf("Failed to encrypt the text!\n");
		return(false);
	}

	if(!image.save_as_png(generate_output_file_path(image_file_path).c_str()))
	{
		printf("Failed to save the image!\n");
	}
	
	return(true);
}

int main(int argc, char** argv)
{
	if(!UTILITIES::IMAGE::initialize())
	{
		printf("\"UTILITIES::IMAGE::initialize()\" failed!\n");
		return(1);
	}

	int exit_code = 0;

	if(argc == 2)	// If a decryption has been requested:
	{
		printf("Pycto is trying to decrypt...\n");
		if(decrypt(argv[1]))
		{
			printf("Pycto successfully decrypted. :D\n");
		}
		else
		{
			printf("Pycto failed to decrypt! D:\n");
			exit_code = 1;
		}
	}
	else if(argc == 3)	// If an encryption has been requested:
	{
		printf("Pycto is trying to encrypt...\n");
		if(encrypt(argv[1], argv[2]))
		{
			printf("Pycto successfully encrypted. :D\n");
		}
		else
		{
			printf("Pycto failed to encrypt! D:\n");
			exit_code = 1;
		}
	}
	else
	{
		printf("Invalid number of arguments!\n\n"
			   "== Usage: ====\n"
			   "-- Decryption: ----\n"
			   "pycto.exe \"file path to image\"\n\n"
			   "-- Encryption: ----\n"
			   "pycto.exe \"file path to image\" \"text to encrypt\"\n");
		exit_code = 1;
	}

	UTILITIES::IMAGE::deinitialize();

	return(exit_code);
}